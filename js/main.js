$(window).load(function(){
	$('.slider').fractionSlider({
		'fullWidth': 			false,
		'controls': 			true, 
		'pager': 				false,
		'responsive': 			false,
		'dimensions': 			"992,470",
	    'increase': 			false,
		'pauseOnHover': 		false,
		'slideEndAnimation': 	true
	});

});